package net.agroop.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * ResponseEntityHelper.java
 * Created by Rúben Madeira on 05/01/2018 - 22:07.
 * Copyright 2018 © eAgroop,Lda
 */
public class ResponseEntityHelper {

    public static ResponseEntity ok() {
        return ResponseEntity.ok(null);
    }

    public static ResponseEntity ok(ResponseStatus status) {
        return ResponseEntity.ok(status);
    }

    public static <T> ResponseEntity ok(T body) {
        return ResponseEntity.ok(body);
    }

    public static ResponseEntity unauthorized() {
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    public static ResponseEntity unauthorized(ResponseStatus status) {
        return new ResponseEntity<>(status, HttpStatus.UNAUTHORIZED);
    }

    public static <T> ResponseEntity unauthorized(T body) {
        return new ResponseEntity<>(body, HttpStatus.UNAUTHORIZED);
    }

    public static ResponseEntity serverError() {
        return serverError(ResponseStatus.withReason("An unexpected error occurred."));
    }

    public static ResponseEntity serverServer(ResponseStatus status) {
        return new ResponseEntity<>(status, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static <T> ResponseEntity serverError(T body) {
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity notAcceptable() {
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
    }

    public static ResponseEntity notAcceptable(ResponseStatus status) {
        return new ResponseEntity<>(status, HttpStatus.NOT_ACCEPTABLE);
    }

    public static <T> ResponseEntity notAcceptable(T body) {
        return new ResponseEntity<>(body, HttpStatus.NOT_ACCEPTABLE);
    }

    public static ResponseEntity badRequest() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity badRequest(ResponseStatus status) {
        return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
    }

    public static <T> ResponseEntity badRequest(T body) {
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity notFound() {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public static ResponseEntity notFound(ResponseStatus status) {
        return new ResponseEntity<>(status, HttpStatus.NOT_FOUND);
    }

    public static <T> ResponseEntity notFound(T body) {
        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }

    public static ResponseEntity conflict() {
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    public static ResponseEntity conflict(ResponseStatus status) {
        return new ResponseEntity<>(status, HttpStatus.CONFLICT);
    }

    public static <T> ResponseEntity conflict(T body) {
        return new ResponseEntity<>(body, HttpStatus.CONFLICT);
    }

    public static ResponseEntity notImplemented() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
