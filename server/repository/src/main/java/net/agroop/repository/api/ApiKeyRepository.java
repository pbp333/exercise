package net.agroop.repository.api;

import net.agroop.domain.api.ApiKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * ApiKeyRepository.java
 * Created by Rúben Madeira on 05/01/2018 - 11:09.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ApiKeyRepository extends JpaRepository<ApiKey,UUID>{
}
