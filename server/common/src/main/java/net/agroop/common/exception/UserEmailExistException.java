package net.agroop.common.exception;

/**
 * UserEmailExistException.java
 * Created by Rúben Madeira on 05/01/2018 - 22:44.
 * Copyright 2018 © eAgroop,Lda
 */
public class UserEmailExistException extends Exception{

    public UserEmailExistException() {}

    public UserEmailExistException(String message) {
        super(message);
    }

    public UserEmailExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserEmailExistException(Throwable cause) {
        super(cause);
    }

    public UserEmailExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}