package net.agroop.common.exception;

/**
 * RegisterException.java
 * Created by Rúben Madeira on 05/01/2018 - 22:17.
 * Copyright 2018 © eAgroop,Lda
 */
public class RegisterException extends Exception{

    public RegisterException() {}

    public RegisterException(String message) {
        super(message);
    }

    public RegisterException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegisterException(Throwable cause) {
        super(cause);
    }

    public RegisterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
