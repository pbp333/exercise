import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import AuthForm from "./AuthForm";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
            <nav className="App-nav">
              <Link to="/">Home</Link>
              <Link to="/registration">Registration</Link>
            </nav>
          </header>
          <p className="App-intro">
            To get started, edit <code>src/App.js</code> and save to reload.
          </p>

          <Switch>
            {/*<Route path="/" component={Home}/>*/}
            <Route path="/registration" component={AuthForm}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
