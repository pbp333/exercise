import React from 'react';
import axios from 'axios';

import config from './config';

/**
 * Form for register one user.
 */
export default class AuthForm extends React.Component {

  state = {
    name: '',
    email: '',
    password: ''
  };

  handleInputChange = e => this.setState({[e.target.name]: e.target.value});

  handleSubmit = (e) => {
    e.preventDefault();

    axios.post(config.serverUrl + '/v1/registration', this.state)
    // Save apiKey to localStorage. Use it on the other requests you make to the server
      .then(data => localStorage.setItem('apiKey', data.apiKey));
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="name" placeholder="Name" onChange={this.handleInputChange}/><br/>
        <input type="email" name="email" placeholder="E-mail" onChange={this.handleInputChange}/><br/>
        <input type="password" name="password" placeholder="Password" onChange={this.handleInputChange}/><br/>
        <button>Create Account</button>
      </form>
    )
  }
}
